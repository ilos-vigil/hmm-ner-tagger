# %% [markdown]
# # NER tagger
# With window feature 2
#
# ## Requirement :
# * Python 3.8
# * Scikit-Learn 0.22.2
# * Numpy 1.18.1
# * NLTK 3.5
# %%
import numpy as np
import gc
from nltk import word_tokenize, pos_tag
from pprint import pprint
from collections import defaultdict
# %% [markdown]
# ## Load dataset
# %%
def load_conll2003_en(path):
    tagged_pos = []
    current_tagged_pos = []

    for s in open(path).readlines():
        if s == '\n' and current_tagged_pos != []:
            tagged_pos.append(current_tagged_pos)
            current_tagged_pos = []
        elif s != '\n':
            pos, tag = s.rstrip('\n').split(' ', maxsplit=2)
            current_tagged_pos.append((pos, tag))

    if current_tagged_pos != []:
        tagged_pos.append(current_tagged_pos)

    return tagged_pos
# %%
dataset = load_conll2003_en('./tags.eng.raw')
dataset
print(f'conll2003 dataset Size : {len(dataset)}')
# %% [markdown]
# ## Preprocessing Step 1
# * Generate train feature (assume window feature is 2)
# * Create list of possible PoS tag
# * Create list of possible NER tag
#
# ### Train feature
# * curr-2 PoS tag
# * curr-1 PoS tag
# * curr PoS tag
# * curr+1 PoS tag
# * curr+2 PoS tag
#
# ### Note for PoS tag
# * `<s>`: prev
# * `</s>` : next
# * `<?>` : unknown PoS tag
# %%
X = []
y = []
list_pos_tag = ['<s>', '</s>', '<?>']
list_ner_tag = []
for sentence in dataset:
    for i in range(len(sentence)):
        # X feature
        curr_m2 = '<s>' if i-2 < 0 else sentence[i-2][0]
        curr_m1 = '<s>'   if i-1 < 0 else sentence[i-1][0]
        curr    = sentence[i][0]
        curr_p1 = '</s>'   if i+1 >= len(sentence) else sentence[i+1][0]
        curr_p2 = '</s>'   if i+2 >= len(sentence) else sentence[i+2][0]
        X.append((
            curr_m2, curr_m1, curr, curr_p1, curr_p2
        ))

        # y
        y.append(sentence[i][1])

        if sentence[i][0] not in list_pos_tag:
            list_pos_tag.append(sentence[i][0])
        if sentence[i][1] not in list_ner_tag:
            list_ner_tag.append(sentence[i][1])

print('List PoS tag:')
pprint(list_pos_tag)
print('List NER tag:')
pprint(list_ner_tag)

del dataset
gc.collect()
# %% [markdown]
# ## Preprocessing Step 2
# * Convert string feature/output to categorical feature
# * Prepare dictionary of PoS/NER tag with it's categorical value
# * https://stackoverflow.com/questions/45321999/how-can-i-optimize-label-encoding-for-large-data-sets-sci-kit-learn
# %%
X = np.array(X)
y = np.array(y)

X_old = X.copy()
y_old = y.copy()

X_key, X = np.unique(X, return_inverse=True)
X = X.reshape((-1, 5))
y_key, y = np.unique(y, return_inverse=True)
y = y.reshape((-1, 1))

print(f'{X.shape=}')
print(f'{y.shape=}')
# %%
dict_pos_tag = defaultdict(lambda : '<?>')
dict_ner_tag = {}

for j in range(len(X_old)):
    for i in range(len(X_old[j])):
        if X_old[j, i] not in dict_pos_tag:
            dict_pos_tag[X_old[j, i]] = X[j, i]

for j in range(len(y_old)):
    if y_old[j, ] not in dict_pos_tag:
        dict_ner_tag[y_old[j, ]] = y[j, ]

del X_old
del y_old
gc.collect()
# from sklearn.preprocessing import LabelEncoder
# le_X = LabelEncoder()
# le_X.fit(list_pos_tag)
# X = [le_X.transform(sentence) for sentence in X]

# le_y = LabelEncoder()
# le_y = le.fit(list_pos_tag)
# y = [le_y.transform(sentence) for sentence in y]
# %% [markdown]
# ## Preprocessing Step 3
# * Convert to numpy
# * Split train and test dataset
# %%
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

print(f'{X_train.shape=}')
print(f'{X_test.shape=}')
print(f'{y_train.shape=}')
print(f'{y_test.shape=}')

del X
del y
gc.collect()
# %% [markdown]
# ## Train
# %%
from sklearn.naive_bayes import CategoricalNB
clf = CategoricalNB()
clf.fit(X_train, y_train)
# %% [markdown]
# ## Test

# %%
y_pred = clf.predict(X_test)

# %%
from sklearn.metrics import classification_report
print(classification_report(y_test, y_pred, target_names=y_key))

# %% [markdown]
# ## Trying with some sentences

# %%
def create_feature(pos_tag):
    X = []
    for i in range(len(pos_tag)):
        # X feature
        curr_m2 = '<s>' if i-2 < 0 else pos_tag[i-2]
        curr_m1 = '<s>' if i-1 < 0 else pos_tag[i-1]
        curr = pos_tag[i]
        curr_p1 = '</s>' if i+1 >= len(pos_tag) else pos_tag[i+1]
        curr_p2 = '</s>' if i+2 >= len(pos_tag) else pos_tag[i+2]

        curr_m2 = dict_pos_tag[curr_m2]
        curr_m1 = dict_pos_tag[curr_m1]
        curr = dict_pos_tag[curr]
        curr_p1 = dict_pos_tag[curr_p1]
        curr_p2 = dict_pos_tag[curr_p2]

        X.append((
            curr_m2, curr_m1, curr, curr_p1, curr_p2
        ))

    return np.array(X)

def search_ner_tag(index):
    for ner_tag, ner_index in dict_ner_tag.items():
        if ner_index == index:
            return ner_tag
    return ''

def tag(sentence):
    print(f'Sentence: {sentence}')

    tokens = word_tokenize(sentence)    
    # print('Tokens:')
    # pprint(tokens)

    tokens_pos = pos_tag(tokens)
    # print('Tokens PoS tag:')
    # pprint(tokens_pos)

    tokens_pos = [tt[1] for tt in tokens_pos]
    tokens_pos = create_feature(tokens_pos)
    ner = clf.predict(tokens_pos)
    # print('Tokens NER tag:')
    # pprint(ner)

    tokens_ner = []
    for i in range(len(tokens)):
        tokens_ner.append((tokens[i], search_ner_tag(ner[i])))
    print('Result:')
    pprint(tokens_ner)

# %%
tag('You were an orphan raised on the streets of the great megatropolises covering Earth.')
tag('She is both pious and driven, the sword in the right hand of the Divine, seeking justice above all else.')
tag('The homeworld and capital of humanity is entering a new golden age.')
tag('Kim is portrayed as calm, quiet, selfless, and by-the-book.')
