# NER tagger

2 NER tagger model created with Scikit-Learn and NLTK library.

## Requirement

* Python 3.8
* NLTK 3.5
* Scikit-Learn 0.22.2
* Numpy 1.18.1

## Corpus

Corpus obtained from https://www.clips.uantwerpen.be/conll2003/ner/

## Python Interactive window

`.py` files was created on VSCode with feature Python Interactive window, see https://code.visualstudio.com/docs/python/jupyter-support-py for more information
