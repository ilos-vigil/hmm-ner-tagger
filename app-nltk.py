# %% [markdown]
# # HMM NER tagger
# %%
from nltk import word_tokenize
from nltk import pos_tag
from nltk.tag.hmm import HiddenMarkovModelTagger
from pprint import pprint
# %% [markdown]
# ## Load conll2003 english corpus
# Corpus obtained from https://www.clips.uantwerpen.be/conll2003/ner/
# %%
def load_conll2003_en(path):
    tagged_pos = []
    current_tagged_pos = []

    for s in open(path).readlines():
        if s == '\n' and current_tagged_pos != []:
            tagged_pos.append(current_tagged_pos)
            current_tagged_pos = []
        elif s != '\n':
            pos, tag = s.rstrip('\n').split(' ', maxsplit=2)
            current_tagged_pos.append((pos, tag))

    if current_tagged_pos != []:
        tagged_pos.append(current_tagged_pos)

    return tagged_pos
# %%
dataset = load_conll2003_en('./tags.eng.raw')
dataset
print(f'conll2003 dataset Size : {len(dataset)}')
# %% [markdown]
# ## Split train and test corpus
# %%
train_percentage = 0.8
train_size = int(train_percentage * len(dataset))
train_data = dataset[:train_size]
test_data = dataset[train_size:]

print(f'Train percentage : {train_percentage*100}%')
print(f'Train Size       : {len(train_data)}')
print(f'Test Size        : {len(test_data)}')
# %% [markdown]
# ## Train HMM
# %%
hmm_model = HiddenMarkovModelTagger.train(train_data)
hmm_model
# %% [markdown]
# ## Test HMM
# May take long time
# %%
acc = hmm_model.test(test_data)
acc
# %% [markdown]
# ## Trying with some sentences
# %%
def tag(sentence):
    print(f'Sentence: {sentence}')

    tokens = word_tokenize(sentence)    
    # print('Tokens:')
    # pprint(tokens)

    tokens_pos = pos_tag(tokens)
    # print('Tokens PoS tag:')
    # pprint(tokens_pos)

    tokens_pos = [tt[1] for tt in tokens_pos]
    pos_ner = hmm_model.tag(tokens_pos)
    # print('Tokens NER tag:')
    # pprint(pos_ner)

    tokens_ner = []
    for i in range(len(tokens)):
        tokens_ner.append((tokens[i], pos_ner[i][1]))
    print('Result:')
    pprint(tokens_ner)
# %%
tag('You were an orphan raised on the streets of the great megatropolises covering Earth.')
tag('She is both pious and driven, the sword in the right hand of the Divine, seeking justice above all else.')
tag('The homeworld and capital of humanity is entering a new golden age.')
tag('Kim is portrayed as calm, quiet, selfless, and by-the-book.')